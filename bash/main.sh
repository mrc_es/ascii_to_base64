#!/usr/bin/env bash

. mappings.sh

declare -a base_64_indexes

export array_characters_str
export chars_to_bin
export base64_result
export equals

declare -a bin_rep_with_padding
declare -a bin_padding_rep_to_dec

map_char_to_bin_representation() {
    local -r string="$1"
    for ((i=0; i < ${#string}; i++)); do
        character="${string:$i:1}"
        chars_to_bin="$chars_to_bin""${char_to_bin_table[$character]}"
    done
}

create_equals() {
    modulo=$(( ${#string_to_convert} % 3))
    [ $modulo -eq 1 ] && amount_equals=2
    [ $modulo -eq 2 ] && amount_equals=1
    [ $modulo -eq 0 ] && amount_equals=0

    [ $amount_equals -ne 0 ] && equals="$(eval "printf '=%.0s' {1..$amount_equals}")"
}

verify_mapping() {

    local -ri string_to_convert_length="${#1}"
    local -ri expected_chars=$((string_to_convert_length*8))
    local -ri chars_to_bin_len="${#2}"
    
    [ $expected_chars -ne $chars_to_bin_len ] && {
        echo >&2 "[ERROR] Expected bytes are:  \"$expected_chars\", but found, \"$((chars_to_bin_len))\" chars"
        exit 1
    }
}

return_with_padding() {
    local -ri reminder="$(( ${#chars_to_bin} % 6 ))"
    local -ri times_to_div="$(( ${#chars_to_bin} / 6 ))"

    offset=0
    for ((i=0; i < times_to_div; i++)); do
        offset=$((6*i))
        bin_rep_with_padding+=("00${chars_to_bin:$offset:6}")
    done
    [ "$reminder" -ne 0 ] && {
        offset=$((offset+6))
        last_padding_chars=$((6-reminder))
        right_padding="$(eval "printf '0%.0s' {1..$last_padding_chars}")"
        bin_rep_with_padding+=("00${chars_to_bin:$offset:$reminder}${right_padding}")
    }
}

convert_bin_padding_rep_to_dec() {
    for padding_rep in ${bin_rep_with_padding[@]}; do
        decimal_rep="${bin_to_dec_index["$padding_rep"]}"
        base_64_indexes+=("$decimal_rep")
    done
}

convert_index_to_base64_str() {
    for index in ${base_64_indexes[@]}; do
        base64_char="${base64_rep[$index]}"
        base64_result="${base64_result}${base64_char}"
    done
}

main() {
    local -r string_to_convert="$1"
    map_char_to_bin_representation "$string_to_convert"
    verify_mapping "$string_to_convert" "$chars_to_bin"
    return_with_padding "$chars_to_bin"
    convert_bin_padding_rep_to_dec
    convert_index_to_base64_str
    create_equals "$string_to_convert"
    echo -e "${base64_result}${equals}"
}

read string_to_convert
main "$string_to_convert"
